import noise
import pygame
from time import sleep
from pygame.locals import *

#settings
terrain_beach_start = 0.36
terrain_plains_start = 0.41
terrain_forest_start = 0.575
terrain_mountains_start = 0.695
terrain_snowymountains_start = 0.75
player_speed = 0.5
camera_zoom = 3.75
zoom_sensitivity = 0.25
terrain_sensitivity = 0.005
screen_height = 500
screen_width = 1000

#vars for later
player_pos = [0,0]

#initalize pygame
print("[EVENT] Starting Pygame...")
pygame.init()
screen=pygame.display.set_mode((screen_width,screen_height),HWSURFACE|DOUBLEBUF|RESIZABLE)
pygame.display.set_caption("Terrain Game v.0.1")
pygame.display.flip()

#make system to color a pixel
def drawpixel(height,pos_x,pos_y):
    use_color = (0,0,0)
    if height > terrain_snowymountains_start:
        use_color = (255,255,255)
    elif height > terrain_mountains_start:
        use_color = (186,186,186)
    elif height > terrain_forest_start:
        use_color = (61,168,49)
    elif height > terrain_plains_start:
        use_color = (46,209,98)
    elif height > terrain_beach_start:
        use_color = (242,204,114)
    else:
        use_color = (36,171,224)
    pos = [pos_x,pos_y]
    pygame.draw.line(screen,use_color,pos,pos,1)

def render():
    for y in range(screen_height):
        for x in range(screen_width):
            # (((OldValue - OldMin) * (NewMax - NewMin)) / (OldMax - OldMin)) + NewMin
            use_y = (((y - 0) * ((player_pos[1] + (camera_zoom / 2)) - (player_pos[1] - (camera_zoom / 2)))) / (screen_height - 0)) + (player_pos[1] - (camera_zoom / 2))
            use_x = (((x - 0) * ((player_pos[0] + (camera_zoom / 2)) - (player_pos[0] - (camera_zoom / 2)))) / (screen_width - 0)) + (player_pos[0] - (camera_zoom / 2))
            drawpixel(noise.pnoise2(use_x,use_y),x,y)
    pygame.display.flip()

def blink():
    screen.fill((255,0,0))
    pygame.display.flip()
    #sleep(0.25)

def exit_game():
    print("[EVENT] Exiting program...")
    pygame.display.quit()
    pygame.quit()
    exit()

#load initial window
print("[INFO] Drawing initial window...")
render()

while True:
    pygame.event.pump()
    event=pygame.event.wait()
    if event.type==QUIT:
        exit_game()
    elif event.type==VIDEORESIZE:
        screen=pygame.display.set_mode(event.dict['size'],HWSURFACE|DOUBLEBUF|RESIZABLE)
        print("[EVENT] Screen resizing to %s x %s..." % (event.dict['size'][0],event.dict['size'][1]))
        screen_width = event.dict['size'][0]
        screen_height = event.dict['size'][1]
        print("[EVENT] Rendering terrain...")
        render()
        print("[EVENT] Rendering Complete.")
    elif event.type==KEYDOWN:
        print("[EVENT] Keypress detected for '%s'." % (event.dict['unicode']))

        # ~~~~~ PLAYER CONTROLS ~~~~~
        # forward
        if event.dict['unicode'] == "w":
            player_pos[1] -= (player_speed * camera_zoom)
            print("[INFO] Player is now at (%s,%s)" % (player_pos[0],player_pos[1]))
        # left
        elif event.dict['unicode'] == "a":
            player_pos[0] -= (player_speed * camera_zoom)
            print("[INFO] Player is now at (%s,%s)" % (player_pos[0],player_pos[1]))
        # down
        elif event.dict['unicode'] == "s":
            player_pos[1] += (player_speed * camera_zoom)
            print("[INFO] Player is now at (%s,%s)" % (player_pos[0],player_pos[1]))
        # right
        elif event.dict['unicode'] == "d":
            player_pos[0] += (player_speed * camera_zoom)
            print("[INFO] Player is now at (%s,%s)" % (player_pos[0],player_pos[1]))

        #   ~~~~~~~~~~~~~~~~~~~~~~~
        # ~~~~~ CAMERA CONTROLS ~~~~~
        #   ~~~~~~~~~~~~~~~~~~~~~~~

        # zoom out
        elif event.dict['unicode'] == "q":
            camera_zoom += zoom_sensitivity
            print("[INFO] Camera zoom is now %sx." % (camera_zoom))
        # zoom in
        elif event.dict['unicode'] == "e":
            camera_zoom -= zoom_sensitivity
            print("[INFO] Camera zoom is now %sx." % (camera_zoom))

        #   ~~~~~~~~~~~~~~~~~~~~~~~
        # ~~~~~ TERRAIN CONTROLS ~~~~~
        #   ~~~~~~~~~~~~~~~~~~~~~~~

        # beach increase
        elif event.dict['unicode'] == "y":
            new_val = terrain_beach_start + terrain_sensitivity
            if new_val < terrain_plains_start:
                terrain_beach_start = new_val
                print("[INFO] Beaches will now generate between %s and %s." % (terrain_beach_start,terrain_plains_start))
            else:
                print("[ERROR] Beaches must be lower than plains!")
                blink()
        # plains increase
        elif event.dict['unicode'] == "u":
            new_val = terrain_plains_start + terrain_sensitivity
            if new_val < terrain_forest_start:
                terrain_plains_start = new_val
                print("[INFO] Plains will now generate between %s and %s." % (terrain_plains_start,terrain_forest_start))
            else:
                print("[ERROR] Plains must be lower than forests!")
                blink()
        # forest increase
        elif event.dict['unicode'] == "i":
            new_val = terrain_forest_start + terrain_sensitivity
            if new_val < terrain_mountains_start:
                terrain_forest_start = new_val
                print("[INFO] Forests will now generate between %s and %s." % (terrain_forest_start,terrain_mountains_start))
            else:
                print("[ERROR] Forests must be lower than mountains!")
                blink()
        # mountains increase
        elif event.dict['unicode'] == "o":
            new_val = terrain_mountains_start + terrain_sensitivity
            if new_val < terrain_snowymountains_start:
                terrain_mountains_start = new_val
                print("[INFO] Mountains will now generate between %s and %s." % (terrain_mountains_start,terrain_snowymountains_start))
            else:
                print("[ERROR] Mountains must be lower than snowy mountains!")
                blink()
        # snowy mountains increase
        elif event.dict['unicode'] == "p":
            terrain_snowymountains_start += terrain_sensitivity
            print("[INFO] Snowy mountains will now generate after %s." % (terrain_snowymountains_start))
        # beach decrease
        elif event.dict['unicode'] == "g":
            terrain_beach_start -= terrain_sensitivity
            print("[INFO] Beaches will now generate between %s and %s." % (terrain_beach_start,terrain_plains_start))
        # plains decrease
        elif event.dict['unicode'] == "h":
            new_val = terrain_plains_start - terrain_sensitivity
            if new_val > terrain_beach_start:
                terrain_plains_start = new_val
                print("[INFO] Plains will now generate between %s and %s." % (terrain_plains_start,terrain_forest_start))
            else:
                print("[ERROR] Plains must be higher than beaches!")
                blink()
        # forest decrease
        elif event.dict['unicode'] == "j":
            new_val = terrain_forest_start - terrain_sensitivity
            if new_val > terrain_plains_start:
                terrain_forest_start = new_val
                print("[INFO] Forests will now generate between %s and %s." % (terrain_forest_start,terrain_mountains_start))
            else:
                print("[ERROR] Forests must be higher than plains!")
                blink()
        # mountains decrease
        elif event.dict['unicode'] == "k":
            new_val = terrain_mountains_start - terrain_sensitivity
            if new_val > terrain_forest_start:
                terrain_mountains_start = new_val
                print("[INFO] Mountains will now generate between %s and %s." % (terrain_mountains_start,terrain_snowymountains_start))
            else:
                print("[ERROR] Mountains must be higher than forests!")
                blink()
        # snowy mountains decrease
        elif event.dict['unicode'] == "l":
            new_val = terrain_snowymountains_start - terrain_sensitivity
            if new_val > terrain_mountains_start:
                terrain_snowymountains_start = new_val
                print("[INFO] Snowy mountains will now generate after %s." % (terrain_snowymountains_start))
            else:
                print("[ERROR] Snowy mountains must be higher than mountains!")
                blink()

        #   ~~~~~~~~~~~~~~~~~~~~~~~
        # ~~~~~ WINDOW CONTROLS ~~~~~
        #   ~~~~~~~~~~~~~~~~~~~~~~~

        # blink
        elif event.dict['unicode'] == "z":
            blink()
        # exit
        elif event.dict['unicode'] == "\x1b":
            exit_game()
        render()

#       ~~~~~~~~~~~~~~~~~~~~~~~
#     ~~~~~ KEYBOARD LAYOUT ~~~~~
#       ~~~~~~~~~~~~~~~~~~~~~~~
#
#  _________________________________________________________________________________________________________________________
#  |           |           |           |           |           |           |           |           |           |           |                                      
#  |     Q     |     W     |     E     |     R     |     T     |     Y     |     U     |     I     |     O     |     P     |
#  |   ZOOM -  |  FORWARD  |   ZOOM +  |           |           |  BEACH +  |  PLAINS + |  FOREST + |   MTNS +  |   SNOW +  |
#  |___________|___________|___________|___________|___________|___________|___________|___________|___________|___________|
#        |           |           |           |           |           |           |           |           |           |                                      
#        |     A     |     S     |     D     |     F     |     G     |     H     |     J     |     K     |     L     |
#        |    LEFT   |  BACKWARD |   RIGHT   |           |  BEACH -  |  PLAINS - |  FOREST - |   MTNS -  |   SNOW -  |
#        |___________|___________|___________|___________|___________|___________|___________|___________|___________|
#              |           |           |           |           |           |           |           |                                      
#              |     Z     |     X     |     C     |     V     |     B     |     N     |     M     |
#              |   BLINK   |           |           |           |           |           |           |
#              |___________|___________|___________|___________|___________|___________|___________|
