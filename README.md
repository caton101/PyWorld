# PyWorld

This is an experiment with terrain generation in Python

![screenshot](Screenshot.png)

## Usage

`python3 Terrain.py`

## Key Bindings

| Key | Purpose                   |
| :-: | :------------------------ |
| w   | move up                   |
| a   | move left                 |
| s   | move down                 |
| d   | move right                |
| z   | blink the window          |
| q   | zoom out                  |
| e   | zoom in                   |
| y   | increase beach height     |
| g   | decrease beach height     |
| u   | increase plains height    |
| h   | decrease plains height    |
| i   | increase forest height    |
| j   | decrease forest height    |
| o   | increase mountains height |
| k   | decrease mountains height |
| p   | increase snow height      |
| l   | decrease snow height      |
