import random
import math
from time import sleep

#settings
x_max = 120
y_max = 36
player_char = "+"
portal_char = "O"
pick_char = "7"
timeout = 0.5
picks_on_board = 20
picks_on_person = 10
refresh_delay = 0.05

#other
level = 0
board = []
picks_left = 0
noise_1 = []
noise_2 = []

def generateWhiteNoise(medium,width,height):
	for y in range(0,height):
		medium.append([])
		for scanline in range(0,width):
			medium[y].append(random.choice(' #'))

#generate new terrain
def newboard():
	global x_max
	global y_max
	global board
	global picks_on_board
	global pick_char
	global noise_1
	global noise_2
	
	#get seed
	num_ = random.random()
	num_ = num_ + 4
	random.seed(num_)
	
	#generate noise channels
	generateWhiteNoise(noise_1,x_max,y_max)
	generateWhiteNoise(noise_2,x_max,y_max)
	
	#compare and compress
	for y in range(y_max):
		board.append([])
		for x in range(x_max):
			compare_1 = noise_1[y][x]
			compare_2 = noise_2[y][x]
			if compare_1 == compare_2:
				board[y].append("#")
			else:
				board[y].append(" ")
	
	#add picks
	for num in range(0,picks_on_board):
		pick_x = random.randint(0,x_max - 1)
		pick_y = random.randint(0,y_max - 1)
		board[pick_y][pick_x] = pick_char

#draw
def draw():
	global x_max
	global board
	global level
	global refresh_delay
	global picks_left
	
	print("==========| Level " + str(level) + " |==========| " + str(picks_left) + " picks left |==========")
	for line in board:
		print("".join(line))
		sleep(refresh_delay)

#place portal
def portal_gen():
	global x_max
	global y_max
	global board
	
	#where will it be
	portal_x = random.randint(0,x_max)
	portal_y = random.randint(0,y_max)
	
	#add portal to board
	temp_line = board[portal_y]
	temp_line[portal_x] = portal_char
	board[portal_y] = temp_line

#make new level
def levelgen():
	global player_x
	global player_y
	global board
	global level
	global picks_on_person
	global picks_left
	
	#reset player position
	player_x = x_max / 2
	player_y = y_max / 2
	player_x = math.floor(player_x)
	player_y = math.floor(player_y)
	player_x = int(player_x)
	player_y = int(player_y)
	
	#reset board
	board = []
	noise_1 = []
	noise_2 = []
	
	#add 1 to level
	level = level + 1
	
	#reset picks
	picks_left = picks_on_person
	
	#make the board
	newboard()
	
	#add portal
	portal_gen()
	
	#add player
	board[player_y][player_x] = player_char
	
	#show the new board
	draw() 

#make character mover
def update_character(direction):
	#use variables outside function
	global player_x
	global player_y
	global portal_char
	global player_char
	global board
	global picks_left
	
	#make old chords
	player_x_old = player_x
	player_y_old = player_y
	
	#make new chords
	if direction == "UP":
		#shift up
		player_y = player_y - 1
	if direction == "DOWN":
		#shift down
		player_y = player_y + 1
	if direction == "LEFT":
		#shift left
		player_x = player_x - 1
	if direction == "RIGHT":
		#shift right
		player_x = player_x + 1
	
	#is the movement possible if not then correct it
	if player_x > (x_max - 1):
		player_x = player_x_old
	if player_x < 0:
		player_x = player_x_old
	if player_y > (y_max - 1):
		player_y = player_y_old
	if player_y < 0:
		player_y = player_y_old
	if board[player_y][player_x] == '#':
		#does player have picks
		if picks_left > 0:
			#go through block
			picks_left = picks_left - 1
		else:
			#access denied
			player_x = player_x_old
			player_y = player_y_old
	
	#will player enter portal
	if board[player_y][player_x] == portal_char:
		#generate new level
		levelgen()
	
	#will player find pick
	if board[player_y][player_x] == pick_char:
		picks_left = picks_left + 1
	
	#remove character from old chords
	temp_list = board[player_y_old]
	temp_list[player_x_old] = ' '
	board[player_y_old] = temp_list
	
	#set character at chords
	temp_list = board[player_y]
	temp_list[player_x] = player_char
	board[player_y] = temp_list
	draw()

#make the first board
levelgen()

#make the game great again
try:
	#game code
	while True:
		#get input
		cmd_raw = input("Enter Command: ")
		cmd_raw.lower()
		
		#command system
		if cmd_raw == "help":
			print("Command - Description")
			print("---------------------")
			print("help - show this prompt")
			print("newlevel - generate a new level")
			print("exit - exit the game")
		elif cmd_raw == "newlevel":
			levelgen()
		elif cmd_raw == "exit":
			print("You completed " + str(level) + " levels.")
			exit()
		else:
			#move player
			for letter in cmd_raw:
				if letter == "w":
					#up
					update_character("UP")
				elif letter == "a":
					#left
					update_character("LEFT")
				elif letter == "s":
					#down
					update_character("DOWN")
				elif letter == "d":
					#right
					update_character("RIGHT")
				sleep(timeout)
except KeyboardInterrupt:
	#exit
	print("You completed " + str(level) + " levels.")
	exit()