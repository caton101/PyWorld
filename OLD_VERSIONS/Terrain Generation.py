import math
import random

#settings
x_max = 100
y_max = 34

#other
board = []
noise_1 = []
noise_2 = []

#get seed
num_ = random.random()
num_ = num_ + 4
random.seed(num_)
print("The seed is: " + str(num_))

#terrain generator
def generateWhiteNoise(medium,width,height):
	for y in range(0,height):
		medium.append([])
		for scanline in range(0,width):
			#noise[i][j] = random.choice(' #')
			medium[y].append(random.choice(' #'))

#write out board
def draw():
	global board
	for line in board:
		print("".join(line))

#compress both noise models
def compare():
	global noise_1
	global noise_2
	global y_max
	global x_max
	global board
	
	for y in range(y_max):
		board.append([])
		for x in range(x_max):
			compare_1 = noise_1[y][x]
			compare_2 = noise_2[y][x]
			if compare_1 == compare_2:
				#add "#"
				board[y].append("#")
			else:
				board[y].append(" ")

#invert board
def invert():
	global board
	for y in range(y_max):
		for x in range(x_max):
			if board[y][x] == " ":
				board[y][x] = "#"
			elif board[y][x] == "#":
				board[y][x] = " "

#tie it all together
try:
	while True:
		cmd = input("Enter command: ")
		if cmd == "help":
			print("Command - Description")
			print("---------------------")
			print("help    - show this list")
			print("gen1    - generates a new noise_1")
			print("gen2    - generates a new noise_2")
			print("all     - generates everything, compares and draws result on screen")
			print("compare - compares noise_1 to noise_2 and generates board")
			print("draw    - draws board")
			print("newseed - generates a new seed")
			print("invert  - invert the symbols on the board")
			print("exit    - exit the program")
		elif cmd == "gen1":
			noise_1 = []
			generateWhiteNoise(noise_1,x_max,y_max)
			print("Noise_1 generated.")
		elif cmd == "gen2":
			noise_2 = []
			generateWhiteNoise(noise_2,x_max,y_max)
			print("Noise_2 generated.")
		elif cmd == "all":
			noise_1 = []
			noise_2 = []
			board = []
			generateWhiteNoise(noise_1,x_max,y_max)
			print("Noise_1 generated.")
			generateWhiteNoise(noise_2,x_max,y_max)
			print("Noise_2 generated.")
			compare()
			print("Board generated.")
			draw()
			print("Done!")
		elif cmd == "compare":
			board = []
			compare()
			print("Board generated")
		elif cmd == "draw":
			draw()
			print("Done!")
		elif cmd == "newseed":
			num_ = random.random()
			num_ = num_ + 4
			random.seed(num_)
			print("The seed is: " + str(num_))
		elif cmd == "invert":
			invert()
		elif cmd == "exit":
			exit()
except KeyboardInterrupt:
	exit()